# remarkjs-slideshows

## Summary

* Slideshows made with remarkjs, mainly for bootcamp classes
	01. Git
	02. Maven
	03. Unit testing with mockito and jasmine
* The templates can be found in the Boilerplates folder, with both online and offline versions.
* There is [webpage version](https://angelesbroullon.gitlab.io/remarkjs-slideshows/) deployed that can be accessed via GitLab pages, so you can see them in action.

## Why?

The current resources were all created with proprietary software, so we needed a free/libre version which may run everywhere.

Remarkjs can be run on any modern browser, hence anyone could access it.


## Credits

Project icon made by [Smashicons](https://www.flaticon.com/authors/smashicons)
The Remarkjs source code can be downloaded from its [GitHub page](https://github.com/gnab/remark).